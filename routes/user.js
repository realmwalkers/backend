const express = require("express")
const router = express.Router()
const crypto = require("crypto")
const nodemailer = require("nodemailer")
const Token = require("../models/Verification")
const User = require("../models/User")
const { check, validationResult } = require("express-validator")
require("dotenv/config")

/*
 * POST /login
 * login with email and password
 */
router.post(
  "/login",
  [
    // Email must be valid
    check("email")
      .isEmail()
      .normalizeEmail(),
    // Password must not be empty
    check("password")
      .not()
      .isEmpty()
  ],
  (req, res) => {
    // Check for validation errors

    var errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).send({ errors: errors.array() })
    }
    User.findOne(
      {
        email: req.body.email
      },
      (err, user) => {
        if (!user) {
          return res.status(401).send({
            msg:
              "The email address " +
              req.body.email +
              " is not associated with any account. Double-check your email address and try again."
          })
        }
        // TODO: comparePassword is not a function. Create function in UserSchema
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (!isMatch) {
            return res.status(401).send({
              msg: "Invalid email or password"
            })
          }
          // Make sure the user has been verified
          if (!user.isVerified) {
            return res.status(401).send({
              type: "not-verified",
              msg: "Your account has not been verified."
            })
          }
          // Create session
          req.session.userId = user._id
          // Login successful, write token, and send back user
          res.send({
            isValidated: true
            // token: generateToken(user),
            // user: user.toJSON()
          })
        })
      }
    )
  }
)

router.get("/logout", (req, res) => {
  if (req.session) {
    req.session.destroy()
  }
})

/*
 * POST /signup
 * sign-up in with email and password
 */
router.post(
  "/signup",
  [
    // Email must be valid
    check("email")
      .isEmail()
      .normalizeEmail(),
    // Password must be at least 8 chars long
    check("password").isLength({ min: 8 })
  ],
  (req, res) => {
    // Check for validation errors
    var errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).send({ errors: errors.array() })
    }
    // Make sure this account doesn't already exist
    User.findOne(
      {
        email: req.body.email
      },
      (err, user) => {
        if (user) {
          return res.status(400).send({
            msg:
              "The email address you have entered is already associated with another account."
          })
        }
        // Create the user
        user = new User({
          email: req.body.email,
          password: req.body.password
        })
        // Save the user in DB
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              msg: err.message
            })
          }

          // Create a verification token for this user
          const token = new Token({
            _userId: user._id,
            token: crypto.randomBytes(16).toString("hex")
          })
          // Save the verification token

          token.save((err) => {
            if (err) {
              return res.status(500).send({ msg: err.message })
            }
            // Send the email
            var transporter = nodemailer.createTransport({
              host: "smtp.sendgrid.net",
              port: 465, // SSL
              secure: true, // use TLS
              auth: {
                user: process.env.SENDGRID_USER,
                pass: process.env.SENDGRID_PASS
              }
            })
            var mailOptions = {
              from: "no-reply@dataTrace.xyz",
              to: user.email,
              subject: "DataTrace: Account Verification Token",
              // TODO: html vs test -> cannot be both?
              text:
                "Hello,\n\n" +
                "Please verify your account by clicking the link: \nhttps://" +
                req.headers.host +
                "/userController/confirmation/" +
                token.token +
                "?email=" +
                user.email +
                ".\n"
              // html: '<b>Hello world?</b>'
            }
            transporter.sendMail(mailOptions, (err) => {
              if (err) {
                return res.status(500).send({
                  sendMail_msg: err.message
                })
              }
              res
                .status(200)
                .send(
                  "A verification email has been sent to " + user.email + "."
                )
            })
          })
        })
      }
    )
  }
)

module.exports = router
