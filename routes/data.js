const express = require("express");
const router = express.Router();
const Data = require("../models/Data");
const tools = require("../tools");

router.get("/", tools.checkSession, (req, res) => {
  // console.log("GET DATA REQUEST");
  Data.findOne(
    {
      _userId: req.session.userId
    },
    (err, userData) => {
      if (!userData) {
        console.log(userData);
        return res.status(400).send({
          msg: "Error with finding data."
        });
      }
      res.send(userData);
    }
  );
});

// POST data handler
router.post("/", tools.checkSession, async (req, res) => {
  Data.findOneAndUpdate(
    {
      _userId: req.session.userId
    },
    (data = {
      // _userId: req.session.userId <- applied automatically per filter
      $push: {
        mental: { score: req.body.mental },
        physical: { score: req.body.physical },
        goal: { score: req.body.goal }
      }
    }),
    {
      // new: true, // returns new data
      upsert: true // If doc does not exist, create one
    },
    (err, userData) => {
      if (err) {
        return res.status(422).send({ msg: err.message });
      }
    }
  );
});

module.exports = router;
