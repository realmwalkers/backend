const express = require("express");
const router = express.Router();
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const Token = require("../models/Verification");
const User = require("../models/User");
const { check, validationResult } = require("express-validator");
require("dotenv/config");

/*
 * POST /confirmation
 */
router.get("/confirmation/:tagId", (req, res) => {
  // Find a matching token
  Token.findOne(
    {
      token: req.params.tagId
    },
    (err, token) => {
      if (!token) {
        return res.status(400).send({
          type: "not-verified",
          msg:
            "We were unable to find a valid token. Your token my have expired."
        });
      }
      // If we found a token, find a matching user
      console.log(req.query.email);
      User.findOne(
        {
          _id: token._userId,
          email: req.query.email
        },
        (err, user) => {
          if (!user) {
            return res.status(400).send({
              msg: "We were unable to find a user for this token."
            });
          }
          if (err) {
            return res.status(400).send({
              msg: err
            });
          }
          if (user.isVerified) {
            return res.status(400).send({
              type: "already-verified",
              msg: "This user has already been verified."
            });
          }
          // Verify and save the user
          user.isVerified = true;
          user.save(err => {
            if (err) {
              return res.status(500).send({
                msg: err.message
              });
            }
            res
              .status(200)
              .send("The account has been verified. Please log in.");
          });
        }
      );
    }
  );
});

/**
 * POST /resend
 * user signed up, but did not validate email
 */
router.post(
  "/resend",
  [
    // Email must be valid
    check("email")
      .isEmail()
      .normalizeEmail()
  ],
  (req, res) => {
    // Check for validation errors
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).send({ errors: errors.array() });
    }
    User.findOne(
      {
        email: req.body.email
      },
      (err, user) => {
        if (!user) {
          return res.status(400).send({
            msg: "We were unable to find a user with that email."
          });
        }
        if (user.isVerified) {
          return res.status(400).send({
            msg: "This account has already been verified. Please log in."
          });
        }
        // Create a verification token, save it, and send email
        var token = new Token({
          _userId: user._id,
          token: crypto.randomBytes(16).toString("hex")
        });
        // Save the token
        token.save(err => {
          if (err) {
            return res.status(500).send({
              msg: err.message
            });
          }
          // Send the email
          var transporter = nodemailer.createTransport({
            host: "smtp.sendgrid.net",
            port: 465, // SSL
            secure: true, // use TLS
            auth: {
              user: process.env.SENDGRID_USER,
              pass: process.env.SENDGRID_PASS
            }
          });
          var mailOptions = {
            from: "no-reply@atomic.me",
            to: user.email,
            subject: "Account Verification Token",
            // TODO: html vs test -> cannot be both?
            text:
              "Hello,\n\n" +
              "Please verify your account by clicking the link: \nhttp://" +
              req.headers.host +
              "/userController/confirmation/" +
              token.token +
              "?email=" +
              user.email +
              ".\n"
            // html: '<b>Hello world?</b>'
          };
          transporter.sendMail(mailOptions, err => {
            if (err) {
              return res.status(500).send({
                msg: err.message
              });
            }
            res
              .status(200)
              .send(
                "A verification email has been sent to " + user.email + "."
              );
          });
        });
      }
    );
  }
);

module.exports = router;
