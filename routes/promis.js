// const express = require("express");
// const router = express.Router();
// const tools = require("../tools");

// // Add new assessment to category
// router.post("/add", tools.checkSession, async (req, res) => {
//   var results = req.body.assessmentResult;
//   var totalDimensionScores = 0;
//   var totalPossible = 0;
//   for (i = 0; i < results.length; i++) {
//     totalDimensionScores += results[i].dimensionScore;
//     totalPossible += 10;
//   }

//   Category.findOneAndUpdate(
//     {
//       _userId: req.session.userId,
//       categoryId: req.body.categoryId
//     },
//     (isAssessment = {
//       $inc: {
//         categoryScorePossible: totalPossible,
//         categoryScoreCurrent: totalDimensionScores
//       },
//       $push: {
//         assessmentsResults: {
//           categoryId: req.body.categoryId,
//           assessmentScore: totalDimensionScores,
//           dimensionResults: req.body.assessmentResult
//         }
//       }
//     }),
//     {
//       new: true // returns new/updated doc
//     },
//     (err, userAssessment) => {
//       if (err) {
//         return res.status(422).send({ msg: err.message });
//       }
//       console.log(req.session.userId);
//       return res.status(200).send("success");
//     }
//   );
// });
