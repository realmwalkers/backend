const express = require("express");
const router = express.Router();
const tools = require("../tools");
const Category = require("../models/Category");

// GET all categories from user
router.get("/", tools.checkSession, async (req, res) => {
  Category.find(
    {
      _userId: req.session.userId
    },
    (err, userCategories) => {
      if (err) {
        return res.status(422).send({ msg: err.message });
      }
      // console.log(userCategories);
      return res.status(200).send(userCategories);
    }
  );
});

// POST new user Category
router.post("/new", tools.checkSession, async (req, res) => {
  console.log(req.body.dimensionList);
  // var dim = req.body.dimArray[0];
  Category.findOneAndUpdate(
    // Check if the document exsists
    {
      _userId: req.session.userId,
      categoryId: req.body.categoryId
    },
    (isCategory = {
      $set: {
        categoryName: req.body.categoryName,
        categoryDescription: req.body.categoryDescription,
        dimensionsTemplate: req.body.dimensionList,
        assessmentsResults: []
      }
    }),
    {
      new: true, // returns new document data
      upsert: true // If doc does not exist, create one
    },
    (err, newUserCategory) => {
      if (err) {
        return res.status(422).send({ msg: err.message });
      }
      return res.status(200).send(newUserCategory);
    }
  );
});

module.exports = router;
