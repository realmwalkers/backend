const express = require("express")
const router = express.Router()
const tools = require("../tools")
const Category = require("../models/Category")

// Add new assessment to category
router.post("/entry", tools.checkSession, async (req, res) => {
  var results = req.body.assessmentResult
  var totalDimensionScores = 0
  var totalPossible = 0
  for (i = 0; i < results.length; i++) {
    totalDimensionScores += results[i].dimensionScore
    totalPossible += 10
  }

  //check for date
  let assessmentDate
  if (req.body.assessmentDate) {
    assessmentDate = req.body.assessmentDate
  } else {
    assessmentDate = new Date()
  }

  Category.findOneAndUpdate(
    {
      _userId: req.session.userId,
      categoryId: req.body.categoryId
    },
    (isAssessment = {
      $inc: {
        categoryScorePossible: totalPossible,
        categoryScoreCurrent: totalDimensionScores
      },
      $push: {
        assessmentsResults: {
          categoryId: req.body.categoryId,
          assessmentComment: req.body.assessmentComment,
          assessmentScore: totalDimensionScores,
          dimensionResults: req.body.assessmentResult,
          assessmentDate: assessmentDate
        }
      }
    }),
    {
      new: true // returns new/updated doc
    },
    (err, userAssessment) => {
      if (err) {
        return res.status(422).send({ msg: err.message })
      }
      // console.log(req.session.userId);
      return res.status(200).send("success")
    }
  )
})

// Get assessment data based on timeframe
router.get(
  "/history",
  // tools.checkSession,
  async (req, res) => {
    const daysParam = req.query.days
    const categoryIdParam = req.query.categoryId

    Category.findOne(
      {
        // _userId: "5dda153f62ac27689cf24992",
        categoryId: categoryIdParam
      },
      (err, userHistory) => {
        if (err) {
          return res.status(422).send({ msg: err.message })
        } else {
          var assessmentHistoryArray = userHistory.assessmentsResults

          sd = new Date().getTime() - daysParam * 24 * 60 * 60 * 1000

          result = assessmentHistoryArray.filter((d) => {
            var time = new Date(d.assessmentDate).getTime()
            return sd < time
          })

          var categoryScoreTime1 = 0
          var categoryScoreTimeTotal = 0
          var lastScore = 0
          var lastScoreTotal = 0
          // console.log(result.length);
          for (i = 0; i < result.length; i++) {
            for (j = 0; j < result[i].dimensionResults.length; j++) {
              categoryScoreTime1 += result[i].dimensionResults[j].dimensionScore
              // console.log(categoryScoreTime1);
              categoryScoreTimeTotal += 10
              if (i === result.length - 1) {
                lastScore += result[i].dimensionResults[j].dimensionScore
                // console.log(
                // "last scores",
                // result[i].dimensionResults[j].dimensionScore
                // );
                // console.log(i);
                // console.log(j);
                lastScoreTotal += 10
              }
            }
          }

          var lastScoreTime = ((lastScore / lastScoreTotal) * 100).toFixed(0)

          var categoryScoreTime = (
            (categoryScoreTime1 / categoryScoreTimeTotal) *
            100
          ).toFixed(0)

          var differenceScoreTime = categoryScoreTime - lastScoreTime

          //
          // TODO: optimizting/refactoring history object build
          //
          newObj = {}
          newObj.categoryId = userHistory.categoryId
          newObj.categoryScorePossible = userHistory.categoryScorePossible
          newObj.categoryScoreCurrent = userHistory.categoryScoreCurrent
          newObj.assessmentsResults = result
          newObj.categoryDescription = userHistory.categoryDescription
          newObj.categoryName = userHistory.categoryName
          newObj.dimensionsTemplate = userHistory.dimensionsTemplate
          newObj.categoryScoreTime = categoryScoreTime
          newObj.lastScoreTime = lastScoreTime
          newObj.differenceScoreTime = differenceScoreTime
          console.log(newObj)
          res.setHeader("Content-Type", "application/json")
          res.status(200).send(newObj)
        }
      }
    )
  }
)

module.exports = router
