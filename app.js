const express = require("express")
const app = express()
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const cors = require("cors")
const session = require("express-session")
require("dotenv/config")

app.use(bodyParser.json())
app.use(
  cors({
    origin: `${process.env.CLIENT_URL}`,
    credentials: true
  })
)

// TODO:
const TWO_HOURS = 1000 * 60 * 60 * 2
// Sessions
app.use(
  session({
    name: "sid",
    resave: false,
    saveUninitialized: false,
    secret: "realmWalkers",
    //store --> configure with mongoDB
    cookie: {
      maxAge: TWO_HOURS, // uses milliseconds
      sameSite: false,
      secure: false, // TODO: make true for production
      httpOnly: false
    }
  })
)

// Import Routes
const userControllerRoute = require("./routes/userController")
const dataRoute = require("./routes/data")
const userRoute = require("./routes/user")
const assessmentRoute = require("./routes/assessment")
const categoryRoute = require("./routes/category")
// const promisRoute = require("./routes/promis");

// Endpoints
app.use("/userController", userControllerRoute)
app.use("/user", userRoute)
app.use("/data", dataRoute)
app.use("/assessment", assessmentRoute)
app.use("/category", categoryRoute)
// app.use("/promis", promisRoute);

// Connent to DB
mongoose.connect(
  process.env.DB_CONNECTION,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
  },
  (err) => console.log("We are connected; ", "Errors:", err)
)

// Use port 3000 unless there exists a preconfigured port
var port = process.env.PORT || 3000

app.listen(port)
