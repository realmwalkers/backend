const fetch = require("node-fetch")
const txtgen = require("txtgen")
const username = "fred@gmail.com"
const password = "fredfred"
var cookie = ""

async function loginUser(email, password) {
  let postBody = {
    email: email,
    password: password
  }
  let validResp = await fetch("http://localhost:3000/user/login", {
    credentials: "include",
    headers: {
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify(postBody)
  })
  // await logRespInfo(validResp);
  setCookie(validResp.headers.get("set-cookie"))
  if (validResp.status.ok) {
    console.log(success)
  }
}

async function createCategoryForUser(catName, catDesc, dimensionsList) {
  let randId = Math.floor(Math.random() * 1000000000)
  let postBody = {
    categoryId: randId,
    categoryName: catName,
    categoryDescription: catDesc,
    dimensionList: dimensionsList
  }
  let dataResp = await fetch("http://localhost:3000/category/new", {
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      Cookie: cookie
    },
    method: "POST",
    body: JSON.stringify(postBody)
  })
  // await logRespInfo(dataResp);
  return randId
}

async function assessCategoryForUser(
  categoryId,
  assessmentResult,
  assessmentComment,
  date
) {
  let postBody = {
    categoryId: categoryId,
    assessmentResult: assessmentResult,
    assessmentComment: assessmentComment,
    assessmentDate: date
  }
  let assessmentResp = await fetch("http://localhost:3000/assessment/entry", {
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      Cookie: cookie
    },
    method: "POST",
    body: JSON.stringify(postBody)
  })
  // await logRespInfo(assessmentResp);
}

function setCookie(cookieStr) {
  cookie = cookieStr
}

async function logRespInfo(fetchResp) {
  console.log(fetchResp.status)
  console.log(await fetchResp.json())
}

function getRandomResults(dimensionNames, date) {
  let results = []
  for (var i = 0; i < dimensionNames.length; i++) {
    if (new Date(date) <= new Date("03/15/2019")) {
      console.log("less than 3/15/2019")
      min = Math.ceil(5)
      max = Math.floor(10)
      dimScore = Math.floor(Math.random() * (max - min + 1)) + min
    } else if (
      new Date(date) > new Date("03/15/2019") &&
      new Date(date) <= new Date("6/20/2019")
    ) {
      console.log("less than 6/20/2019")
      min = Math.ceil(8)
      max = Math.floor(4)
      dimScore = Math.floor(Math.random() * (max - min + 1)) + min
    } else if (
      new Date(date) > new Date("6/20/2019") &&
      new Date(date) <= new Date("10/31/2019")
    ) {
      console.log("less than 10/31/2019")
      min = Math.ceil(7)
      max = Math.floor(2)
      dimScore = Math.floor(Math.random() * (max - min + 1)) + min
    } else if (
      new Date(date) > new Date("10/31/2019") &&
      new Date(date) <= new Date("11/12/2019")
    ) {
      console.log("less than 11/12/2019")
      min = Math.ceil(6)
      max = Math.floor(10)
      dimScore = Math.floor(Math.random() * (max - min + 1)) + min
    } else if (
      new Date(date) > new Date("11/12/2019") &&
      new Date(date) <= new Date("12/30/2020")
    ) {
      min = Math.ceil(7)
      max = Math.floor(10)
      dimScore = Math.floor(Math.random() * (max - min + 1)) + min
    } else {
      console.log("skipping")
      dimScore = Math.floor(Math.random() * 10) + 1
    }
    let randResult = {
      dimensionName: dimensionNames[i],
      dimensionScore: dimScore
    }
    results.push(randResult)
  }
  return results
}

function getRandomComment() {
  return txtgen.sentence()
}

async function bulkAssess(numAssessments, categoryId, startDate, endDate) {
  var date = startDate
  for (var i = 0; i < numAssessments; i++) {
    console.log("bulk assess")
    let dimensionNames = ["Exercise", "Diet"]
    let assessmentResult = getRandomResults(dimensionNames, date)
    let assessmentComment = getRandomComment()
    await assessCategoryForUser(
      categoryId,
      assessmentResult,
      assessmentComment,
      date
    )

    console.log("date =", date)
    if (new Date(date) >= new Date(endDate)) {
      numAssessments = -1
    } else if (new Date(date) > new Date("01/26/2020")) {
      randomNum = Math.floor(Math.random() * Math.floor(4)) + 1 //random number between 1 and 5
      console.log("random num gen = ", randomNum)
      var dayInterval = 0
      for (var i = 0; i <= randomNum; i++) {
        dayInterval += 86400000
      }
      console.log("interval =", dayInterval)
      date = new Date(date.getTime() + dayInterval) //increment day
    } else {
      randomNum = Math.floor(Math.random() * Math.floor(12)) + 1 //random number between 1 and 5
      console.log("random num gen = ", randomNum)
      var dayInterval = 0
      for (var i = 0; i <= randomNum; i++) {
        dayInterval += 86400000
      }
      console.log("interval =", dayInterval)
      date = new Date(date.getTime() + dayInterval) //increment day
      numAssessments -= randomNum
    }
    console.log("assessments =", numAssessments)
    // date = new Date(date.getTime() + 86400000); //increment day
  }
}

async function genData() {
  await loginUser(username, password)
  let categoryName = "Resolutions"
  let categoryDesc = "New Year's Resolutions"
  let dimensionsList = [
    { dimensionName: "Exercise", dimensionDiscription: "Exercise quality" },
    { dimensionName: "Diet", dimensionDiscription: "Diet constancy" }
  ]
  let categoryId = await createCategoryForUser(
    categoryName,
    categoryDesc,
    dimensionsList
  )
  let numAssessments = 365
  let startDate = new Date("01/01/2019")
  let endDate = new Date("01/26/2020")
  await bulkAssess(numAssessments, categoryId, startDate, endDate)
}

genData()
