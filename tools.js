module.exports = {
  // Check if a session exists, if not send message to login
  checkSession: (req, res, next) => {
    if (!req.session.userId) {
      res.status(401).send({
        msg: "Not authorized to access"
      });
    } else {
      next();
    }
  }
};
