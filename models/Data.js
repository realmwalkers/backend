const mongoose = require("mongoose");

// Data points
const DataSchema = mongoose.Schema({
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User"
  },
  mental: [
    {
      date: {
        type: Date,
        default: Date.now
      },
      score: {
        type: Number,
        require: true
      },
      comment: String
    }
  ],
  physical: [
    {
      date: {
        type: Date,
        default: Date.now
      },
      score: {
        type: Number,
        require: true
      },
      comment: String
    }
  ],
  goal: [
    {
      date: {
        type: Date,
        default: Date.now
      },
      score: {
        type: Number,
        require: true
      },
      comment: String
    }
  ]
});

module.exports = mongoose.model("Data", DataSchema);
