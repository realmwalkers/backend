const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

// User info
const UserSchema = mongoose.Schema({
  email: {
    type: String,
    lowercase: true,
    required: true,
    unique: true
  },
  password: {
    type: String,
    require: true
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  passwordRestToken: { type: String },
  passwordResetExpires: { type: Date }
});

/*
 * ORM Methods
 * Pre-save of user to database, hash password if password is modified or new
 */
UserSchema.pre("save", function(next) {
  const user = this,
    SALT_FACTOR = 5;
  if (!user.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

// Method to compare password for login
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("User", UserSchema);
