const mongoose = require("mongoose");
const Dimensions = require("../models/Dimensions").schema;

// An "Assessment" corresponds to a "Category"
// An assessment wrappes the sum dimensions --> see DimensionsSchema
const AssessmentSchema = mongoose.Schema({
  // _assessmentId will be paired to
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User"
  },
  categoryId: {
    type: Number,
    require: true
  },
  assessmentScore: {
    // Total from Dimensions score
    type: Number
    // require: true
  },
  assessmentDate: {
    type: Date,
    default: Date.now
  },
  assessmentComment: {
    type: String,
    maxLength: 240,
    default: "this is a test comment"
  },
  dimensionResults: [Dimensions]
});

module.exports = mongoose.model("Assessment", AssessmentSchema);
