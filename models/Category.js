const mongoose = require("mongoose");
const Assessment = require("./Assessment").schema;
const Dimensions = require("./Dimensions").schema;

// A "Category" is the label to an objective.
// ex. Fitness is a category
// a user can have many (>= 5) cateories
// A Category has a Assessments -> see Assessment Schema
// So Categories correlates to many Assessments i.e. assessmentArray
// TODO: Category startDate and endDate
const CategorySchema = mongoose.Schema({
  // _categoryId will be the same as CatCatergy, which only is one per user
  // _categoryId will link all categories to a catalog
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User"
  },
  categoryId: {
    type: Number,
    require: true
  },
  active: {
    type: Boolean,
    default: true
  },
  categoryName: {
    type: String,
    // require: true,
    maxLength: 25
  },
  categoryScorePossible: {
    type: Number,
    default: 0
  },
  categoryScoreCurrent: {
    type: Number,
    default: 0
  },
  categoryDescription: {
    type: String,
    maxLength: 240
  },
  dimensionsTemplate: [Dimensions],
  assessmentsResults: [Assessment]
});

module.exports = mongoose.model("Category", CategorySchema);
