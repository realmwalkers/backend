const mongoose = require("mongoose");

// Many "Dimensions" corresponds to an "Assessment".
// ex. Fitness as Assessmet, will have eating, workout, sleep as Dimensions
const DimensionsSchema = mongoose.Schema({
  // _dimensionsId: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   required: true,
  //   ref: "Assessment"
  // },
  dimensionName: {
    type: String
    // default: "something different"
  },
  dimensionDiscription: {
    type: String,
    maxLength: 140
  },
  dimensionQuestion: {
    type: String
  },
  dimensionScore: {
    type: Number
    // default: 1,
    // require: true,
    // min: 1,
    // max: 10
  }
});

module.exports = mongoose.model("Dimensions", DimensionsSchema);
